/* eslint-disable import/no-dynamic-require, global-require */

const path = require('path')
const merge = require('lodash/merge')
const appRoot = require('app-root-path')

const defaults = {
  stories: [`${appRoot}/stories/**/*.stories.@(js|md|mdx)`],
  addons: [
    {
      name: '@storybook/addon-essentials',
      options: {
        backgrounds: false,
      },
    },
  ],
  core: {
    builder: 'webpack5',
  },
}

let main

try {
  const mainOverrides = require(`${appRoot}/.storybook/main.js`)
  /* eslint-disable-next-line no-console */
  console.log('Custom main.js file found')

  /**
   * Paths are relative to ./storybook/main.js to keep compatibility with how
   * storybook normally works. This maps the relative paths to absolute ones
   * so that they get picked up.
   */
  if (mainOverrides.stories && Array.isArray(mainOverrides.stories)) {
    mainOverrides.stories = mainOverrides.stories.map(relativePath =>
      path.resolve(`${appRoot}/.storybook/${relativePath}`),
    )
  }

  main = merge(defaults, mainOverrides)
} catch (e) {
  // console.error(e)
  console.warn('Local overrides for main.js not found. Applying defaults')
  main = defaults
}

module.exports = main
