/* eslint-disable import/prefer-default-export */

import React from 'react'
import { addDecorator } from '@storybook/react'
import { createGlobalStyle, ThemeProvider } from 'styled-components'
import { MemoryRouter } from 'react-router-dom'
import get from 'lodash/get'
import pickBy from 'lodash/pickBy'

// eslint-disable-next-line import/no-unresolved
import theme from 'theme' // defined as alias in webpack
import { ConfigProvider } from 'antd'

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${props => props.theme.colorBackground};
    color: ${props => props.theme.colorText};
    font-family: ${props => props.theme.fontInterface}, sans-serif;
    font-size: ${props => props.theme.fontSizeBase};
    line-height: ${props => props.theme.lineHeightBase};

    * {
      box-sizing: border-box;
    }
  }
`
const pxToNumConverter = value => {
  if (typeof value === 'string') {
    if (value.slice(-2) === 'px') return parseInt(value.slice(0, -2), 10)
  }

  return value
}

const mapper = {
  borderRadius: pxToNumConverter(theme.borderRadius),
  colorBgBase: theme.colorBackground,
  colorTextBase: theme.colorText,
  fontFamily: theme.fontInterface,
  fontSize: pxToNumConverter(theme.fontSizeBase),
  fontSizeHeading1: pxToNumConverter(theme.fontSizeHeading1),
  fontSizeHeading2: pxToNumConverter(theme.fontSizeHeading2),
  fontSizeHeading3: pxToNumConverter(theme.fontSizeHeading3),
  fontSizeHeading4: pxToNumConverter(theme.fontSizeHeading4),
  fontSizeHeading5: pxToNumConverter(theme.fontSizeHeading5),
  fontSizeHeading6: pxToNumConverter(theme.fontSizeHeading6),
  lineType: theme.borderStyle,
  lineWidth: pxToNumConverter(theme.borderWidth),
  motionUnit: theme.transitionDuration,
  sizeUnit: pxToNumConverter(theme.gridUnit),
}

const filtered = pickBy(mapper, v => !!v)

const mappedAntTheme = {
  token: {
    ...theme,
    ...filtered,
  },
}

addDecorator(Story => (
  <ConfigProvider theme={mappedAntTheme}>
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Story />
    </ThemeProvider>
  </ConfigProvider>
))

addDecorator(Story => (
  <MemoryRouter>
    <Story />
  </MemoryRouter>
))

/*
  Run local preview.js file from app
*/

let previewFileParameters = {}

try {
  // Load ./storybook/preview.js
  // eslint-disable-next-line import/no-unresolved, global-require
  const preview = require('appPreview') // defined as alias in webpack

  // Grab and re-export the parameters object if it exists
  const params = get(preview, 'parameters')
  if (params) previewFileParameters = params
} catch (e) {
  console.error(e)
}

const parameters = previewFileParameters

export { parameters }
