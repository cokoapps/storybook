const path = require('path')
const fs = require('fs')

const cloneDeep = require('lodash/cloneDeep')
const appRoot = require('app-root-path')

const customThemePath = process.env.STORYBOOK_THEME_PATH
const customUiFolderPath = process.env.CLIENT_UI_FOLDER_PATH

module.exports = ({ config }) => {
  const clonedConfig = cloneDeep(config)

  // make sure preview.js is included
  const jsInclude = clonedConfig.module.rules[0].include
  jsInclude.push(path.resolve(__dirname, 'preview.js'))

  // make sure @coko/storybook is not excluded, as it comes without a dist folder
  // eslint-disable-next-line func-names
  clonedConfig.module.rules[0].exclude = function (modulePath) {
    return (
      /node_modules/.test(modulePath) &&
      !/node_modules\/@coko\/storybook/.test(modulePath)
    )
  }

  const mjsRuleFix = {
    test: /\.m?js/,
    resolve: {
      fullySpecified: false,
    },
  }

  clonedConfig.module.rules = [mjsRuleFix, ...clonedConfig.module.rules]

  // look for local .storybook/preview.js file in project
  const localPreviewPath = `${appRoot}/.storybook/preview.js`
  const localPreviewExists = fs.existsSync(localPreviewPath)

  if (localPreviewExists) {
    /* eslint-disable-next-line no-console */
    console.log(`Local preview file will be loaded from ${localPreviewPath}`)
  } else {
    /* eslint-disable-next-line no-console */
    console.log('No custom preview file found')
  }

  // find ui folder path (so that you can import from 'ui')
  const uiFolderPath = customUiFolderPath
    ? `${appRoot}/app/${customUiFolderPath}`
    : `${appRoot}/app/ui`

  // look for theme file
  const themePath = customThemePath
    ? `${appRoot}/${customThemePath}`
    : `${appRoot}/app/theme.js`
  const themeExists = fs.existsSync(themePath)

  // eslint-disable-next-line import/no-dynamic-require, global-require
  // const theme = require(themePath)

  if (!themeExists) {
    throw new Error(`Theme file not found at ${themePath}!`)
  } else {
    /* eslint-disable-next-line no-console */
    console.log(`Theme will be loaded from ${themePath}`)
  }

  // define aliases so that you don't have to deal with dynamic imports
  clonedConfig.resolve.alias.appPreview = localPreviewPath
  clonedConfig.resolve.alias.theme = themePath
  clonedConfig.resolve.alias.ui = uiFolderPath

  // handle .storybook/preview.js file not existing gracefully
  clonedConfig.resolve.fallback.appPreview = path.resolve(__dirname, 'noop')

  clonedConfig.resolve.fallback.assert = require.resolve('assert')

  return clonedConfig
}
