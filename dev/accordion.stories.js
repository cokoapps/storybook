/* eslint-disable react/jsx-props-no-spreading */

import React from 'react'
import { Collapse } from 'antd'

export const Accordion = () => {
  return (
    <Collapse accordion>
      <Collapse.Panel header="Panel 1" key={1}>
        Panel 1 content
      </Collapse.Panel>
      <Collapse.Panel header="Panel 2" key={2}>
        Panel 2 content
      </Collapse.Panel>
      <Collapse.Panel header="Panel 3" key={3}>
        Panel 3 content
      </Collapse.Panel>
    </Collapse>
  )
}

export default {
  component: Accordion,
  title: 'Accordion',
}
