/* eslint-disable react/jsx-props-no-spreading */

import React from 'react'
import { Select as AntSelect } from 'antd'
import styled from 'styled-components'

const StyledSelect = styled(AntSelect)`
  width: 300px;
`

export const Select = () => {
  return (
    <StyledSelect
      options={[
        { value: '1', label: 'option 1' },
        { value: '2', label: 'option 2' },
      ]}
      placeholder="Select an option"
    />
  )
}

export default {
  component: Select,
  title: 'Select',
}
