export default {
  colorPrimary: 'olive',
  colorTextReverse: 'white',
  // fontSize: '16',
  fontSizeBase: '16px',
  lineHeight: '1.5',
}
