/* eslint-disable react/jsx-props-no-spreading */

import React from 'react'
import PropTypes from 'prop-types'
import { th } from '@coko/client'
import { Button as AntButton } from 'antd'
import styled, { css } from 'styled-components'

const StyledButton = styled(AntButton)`
  color: ${th('colorPrimary')};

  ${props =>
    props.type === 'primary' &&
    css`
      background-color: ${th('colorPrimary')};
      color: ${th('colorTextReverse')};
    `}
`

const Button = props => {
  const { children, onClick, ...rest } = props
  return (
    <StyledButton onClick={onClick} {...rest}>
      {children}
    </StyledButton>
  )
}

Button.propTypes = {
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func,
}

Button.defaultProps = {
  onClick: () => {
    console.log('click click')
  },
}

export const NormalButton = () => {
  return <Button>Click me!</Button>
}

export const PrimaryButton = () => {
  return <StyledButton type="primary">Click me</StyledButton>
}

export default {
  component: Button,
  title: 'Button',
}
