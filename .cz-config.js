const { commitizen } = require('@coko/lint')

commitizen.scopes = ['*']

module.exports = commitizen
