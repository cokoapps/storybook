const { addDecorator } = require('@storybook/react')
/* eslint-disable-next-line import/no-extraneous-dependencies */
const blocks = require('@storybook/addon-docs/blocks') // part of essentials

module.exports = { addDecorator, ...blocks }
