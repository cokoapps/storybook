# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.0.0](https://gitlab.coko.foundation///compare/v1.7.1...v2.0.0) (2022-12-23)


### Features

* upgrade to ant v5 ([1b0c04e](https://gitlab.coko.foundation///commit/1b0c04e549ac84aaa3d788466f02e6893ec8cf6d))

### [1.7.1](https://gitlab.coko.foundation///compare/v1.7.0...v1.7.1) (2022-09-02)


### Bug Fixes

* allow import statements in theme ([5e7a346](https://gitlab.coko.foundation///commit/5e7a34614a84ad9591958835fa65d73341971689))

## [1.7.0](https://gitlab.coko.foundation///compare/v1.6.0...v1.7.0) (2022-09-01)


### Features

* upgrade to storybook 6.5.10 ([a586ce3](https://gitlab.coko.foundation///commit/a586ce3f0ae694266beba8c1c97c12c13a28dd88))

## [1.6.0](https://gitlab.coko.foundation///compare/v1.5.0...v1.6.0) (2021-05-27)


### Features

* allow relative paths in story globs ([e6480a4](https://gitlab.coko.foundation///commit/e6480a45ddfb0cf0c6298794db48e25e83a58178))

## [1.5.0](https://gitlab.coko.foundation///compare/v1.4.1...v1.5.0) (2021-05-08)


### Features

* add less support ([7170d21](https://gitlab.coko.foundation///commit/7170d2190ea625f3a2fdf52629faade61ad926e6))

### [1.4.1](https://gitlab.coko.foundation///compare/v1.4.0...v1.4.1) (2021-04-29)


### Bug Fixes

* handle custom theme path correctly ([f958b60](https://gitlab.coko.foundation///commit/f958b606058e6bad67917d8afc388af3de30c5ac))

## [1.4.0](https://gitlab.coko.foundation///compare/v1.3.1...v1.4.0) (2021-04-29)


### Features

* allow re-export of parameters in preview.js ([faa05ee](https://gitlab.coko.foundation///commit/faa05ee640c9150ffadfc211c6e16c10e971f6a2))


### Bug Fixes

* provide feedback on files found in logs ([4766b88](https://gitlab.coko.foundation///commit/4766b88b3683df443ebd22654dc9fcb010e6672c))

### [1.3.1](https://gitlab.coko.foundation///compare/v1.3.0...v1.3.1) (2021-04-29)


### Bug Fixes

* handle optional files gracefully ([a9752d4](https://gitlab.coko.foundation///commit/a9752d471ca3f789c0046a8d054cac8636a9e85d))

## [1.3.0](https://gitlab.coko.foundation///compare/v1.2.2...v1.3.0) (2021-04-14)


### Features

* webpack 5 support ([865cf3d](https://gitlab.coko.foundation///commit/865cf3d29955d0f022421204e385b628d1608386))


### Bug Fixes

* fix webpack config for webpack 5 support ([c6196d4](https://gitlab.coko.foundation///commit/c6196d423b31f0718466597e0aa9d7ed4524a2f3))
* turn backgrounds addon off ([f85cb9d](https://gitlab.coko.foundation///commit/f85cb9d6029b18c4259d887cdface3f871fc1b66))

### [1.2.2](https://gitlab.coko.foundation///compare/v1.2.1...v1.2.2) (2021-01-26)


### Bug Fixes

* make runner script mac compatible ([fb3429f](https://gitlab.coko.foundation///commit/fb3429f6f0040fdaa615429301394fdb5fe6fd5c))

### [1.2.1](https://gitlab.coko.foundation///compare/v1.2.0...v1.2.1) (2020-12-20)

## [1.2.0](https://gitlab.coko.foundation///compare/v1.1.0...v1.2.0) (2020-12-20)


### Features

* export docs blocks ([afd875c](https://gitlab.coko.foundation///commit/afd875c96dbb6302427b17c08b1552e1b2b3e305))


### Bug Fixes

* fix stories default file glob ([d54c595](https://gitlab.coko.foundation///commit/d54c595041042ca698458741127b66919ecb26c3))

## [1.1.0](https://gitlab.coko.foundation///compare/v1.0.0...v1.1.0) (2020-12-19)


### Features

* read md & mdx files in default folder ([4195234](https://gitlab.coko.foundation///commit/41952348f8bd976829c45e9b642b1d8590455bad))

## [1.0.0](https://gitlab.coko.foundation///compare/v0.1.6...v1.0.0) (2020-12-19)

### [0.1.6](https://gitlab.coko.foundation///compare/v0.1.5...v0.1.6) (2020-12-18)


### Bug Fixes

* fix preview.js not being parsed by babel ([57cb517](https://gitlab.coko.foundation///commit/57cb517b34839795511712ced76c2e01fe64f58d))

### [0.1.5](https://gitlab.coko.foundation///compare/v0.1.4...v0.1.5) (2020-12-18)


### Features

* make package zero config ([45240fe](https://gitlab.coko.foundation///commit/45240fe4bd9455ed8567c02cd67795a6db54676c))

### [0.1.4](https://gitlab.coko.foundation///compare/v0.1.3...v0.1.4) (2020-10-26)


### Features

* remove docs arg from cli to show canvas again ([e612e3a](https://gitlab.coko.foundation///commit/e612e3a953c9107ce6e99e96e44736e312b7f7f0))

### [0.1.3](https://gitlab.coko.foundation///compare/v0.1.2...v0.1.3) (2020-10-23)


### Features

* add storybook essentials ([70e8e62](https://gitlab.coko.foundation///commit/70e8e62eb8f92684b4fc8ac0bbd121fa813a9044))

### [0.1.2](https://gitlab.coko.foundation///compare/v0.1.1...v0.1.2) (2020-10-20)


### Features

* add export for addDecorator ([b15028d](https://gitlab.coko.foundation///commit/b15028d079f54d545d7150b210e79460d6b521d1))

### 0.1.1 (2020-07-07)


### Features

* provide a main.js file ([4e8ebb0](https://gitlab.coko.foundation///commit/4e8ebb04b355c1786fa174cd1f03900a4742eb4e))
